# csv_downloader
Download CSV file and save it line by line to PostgreSQL.

## Requirements:
psycopg2

## Usage:
```
./download_csv.py {postgres connection info} {url},
```
where
	{postgres connection info} - psql like options
	{url}                      - url pointing to csv file

## Example

```
./download_csv.py -Utest -P5432 -hlocalhost -dtest_db -p12345 http://example/file.csv
```

or with local file and default connection info:
```
./download_csv.py file:///home/foo/bar.csv
```
