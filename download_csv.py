#!/usr/bin/env python3

import sys
import csv
import json
import getopt

import psycopg2 as pg
import psycopg2.extras as pg_ext

from urllib.request import urlopen

CHUNK_SIZE = 1024 ** 2
ROWS_PER_INSERT = 1000


def csv_download(url, chunk_size=CHUNK_SIZE):
    buff = ''
    header = None

    with urlopen(url) as res:
        while True:
            bin_data = res.read(chunk_size)

            if not (bin_data or buff):
                break

            data = ''.join([buff, bin_data.decode('utf-8', 'replace')]).splitlines(True)
            buff = data.pop() if bin_data else ''

            for line in csv.reader(data, delimiter=',', quotechar='"'):
                stripped = (x.strip() for x in line)

                if not header:
                    header = list(stripped)
                    continue

                yield dict(zip(header, stripped))


def bulk_insert(cur, data):
    pg_ext.execute_values(cur, 'INSERT INTO test_data(data) VALUES %s', data)


def csv_load_and_save(url, pg_conn):
    cur = pg_conn.cursor()

    bulk = []
    for json_data in csv_download(url):
        bulk.append((json.dumps(json_data),))

        if len(bulk) > ROWS_PER_INSERT:
            bulk_insert(cur, bulk)
            bulk = []

    if bulk:
        bulk_insert(cur, bulk)

    pg_conn.commit()


def get_options():
    if len(sys.argv) < 2:
        sys.exit('wrong params')

    args = sys.argv[1:]
    optlist, args = getopt.getopt(args, 'p:h:U:d:P:')

    if not len(args):
        sys.exit('url required')

    optdict = dict(optlist)

    info = {
        'database': optdict.get('-d') or 'python_test',
        'user': optdict.get('-U') or 'test',
        'host': optdict.get('-h') or 'localhost',
        'port': optdict.get('-P') or 5432,
        'password': optdict.get('-p') or ''
    }

    return info, args[0]


if __name__ == '__main__':
    pg_conn_info, url = get_options()

    with pg.connect(**pg_conn_info) as pg_conn:
        csv_load_and_save(url, pg_conn)
